require_relative 'filosofos'

num_garfos = 5

Filosofos = ['Socrates', 'Platão', 'Aristoteles', 'Friedrich', 'Karl Marx']

Fork = Struct.new(:fork_id, :mutex)
forks = Array.new(num_garfos) { |i| Fork.new(i, Object.new.extend(Mutex_m)) }

Mesa = Array.new(num_garfos) do |i|

  Thread.new(Filosofos[i], forks[i], (forks[(i + 1) % num_garfos])) do |id_thread, garfo_esquerda, garfo_direita|

    filosofo = Filosofo.new(id_thread, garfo_esquerda, garfo_direita).iniciar

  end
end

Mesa.each { | thread | thread.join }
