require 'mutex_m'

# Classe de um filosofo
class Filosofo
  def initialize(nome, garfo_esquerda, garfo_direita)
    @nome = nome
    @garfo_esquerda = garfo_esquerda
    @garfo_direita = garfo_direita
    @comida = 0
  end

  def iniciar
    while @comida < 5
      pensar
      comer
    end
    p "Filosofo #@nome comeu toda a sua comida!\n"
  end

  def pensar

    p "Filosofo #@nome esta pensando\n"

    sleep(rand)

    p "Filosofo #@nome esta pensando\n"

  end

  def comer
    garfo_esq = @garfo_esquerda

    garfo_dir = @garfo_direita

    loop do

      pegar(garfo_esq, esperar: true)

      p "Filosofo #@nome pegou o garfo #{ garfo_esq.fork_id }\n"

      break if pegar(garfo_dir, esperar: false)

      p "Filosofo #@nome não pode pegar o segundo garfo #{ garfo_dir.fork_id }\n"
      soltar(garfo_esq)

      garfo_esq, garfo_dir = garfo_dir, garfo_esq

    end

    p "Filosofo #@nome pegou o segundo garfo #{ garfo_dir.fork_id }\n"

    p "Filosofo #@nome esta comendo...\n"

    sleep(rand)

    @comida += 1

    soltar(@garfo_esquerda)

    soltar(@garfo_direita)

  end

  def pegar(garfo, opcao)
    p "Filosofo #@nome tentou pegar o garfo #{garfo.fork_id}\n"
    opcao[:esperar] ? garfo.mutex.mu_lock : garfo.mutex.mu_try_lock
  end

  def soltar(garfo)
    p "Filosofo #@nome soltou o garfo #{garfo.fork_id}\n"
    garfo.mutex.unlock
  end
end

